
jQuery(document).ready(function(){

  jQuery(".main-text").slick({
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000
  });

  jQuery.stellar();

});